import { Pacijent } from '../models';
import {  PostgresDAO } from '../postgresDao';
import {DAO} from '../dao';
import { MockDB } from '../mockdb';
import {API} from '../api'
import { Service } from '../service';
var pacientId;

describe('testing postgres dao', () => {

  
  test('create Pacient',  () => {
    console.log("1");
    var dao = new PostgresDAO();
        dao.getPacientsBasic().then((res)=>{
          var oldLen = (res as Pacijent[]).length
          dao.createBasicPacient("testingUnit");
          dao.getPacientsBasic().then((res)=>{
            pacientId = (res as Pacijent[]).find(p => p.statusZuba == "testingUnit")?.id;
            expect((res as Pacijent[]).length).toBe(oldLen+1);
          })
        })
  });
  test('Edit pacient', async () => {
    console.log("2");
    var dao = new PostgresDAO();  
    dao.updatePacientById(pacientId, "newTestingUnit")
    dao.getPacientById(pacientId).then(res =>{
      expect((res as Pacijent).statusZuba).toBe("newTestingUnit");
    })
  });
  test('Delete pacient', async () => {
    console.log("3");
    var dao = new PostgresDAO();
        dao.getPacientsBasic().then((res)=>{
          var oldLen = (res as Pacijent[]).length
          dao.deletePacientById(pacientId);
          dao.getPacientsBasic().then((res)=>{
            pacientId = (res as Pacijent[]).find(p => p.statusZuba == "testingUnit")?.id;
            expect((res as Pacijent[]).length).toBe(oldLen-1);
          })
        })
  });
});

describe('testing service', () => {

  test('create Pacient',  () => {
    console.log("1");
    var dao = new MockDB();
    var oldL = 0
    dao.createBasicPacient("status");
    var newL = dao.getPacientsBasic().length
    expect(newL).toBe(1);
  });
  test('Edit pacient', async () => {
    console.log("2");
    var dao = new MockDB();

    dao.createBasicPacient("status");
    dao.updatePacientById(0, "novistatus");
    expect((dao.getPacientById(0) as Pacijent).statusZuba).toBe("novistatus");
  });
  test('Delete pacient', async () => {
    console.log("3");
    console.log("1");
    var dao = new MockDB();

    dao.createBasicPacient("status");
    var oldL = dao.getPacientsBasic().length;
    dao.deletePacientById(0);
    var newL = dao.getPacientsBasic().length;
    expect(newL).toBe(oldL-1);
  });
});
