
class Korisnik{
    oib:string;
    ime:string;
    prezime:string;
    email:string;
    mjesto:Mjesto;
    password:string|null;
    dentistId:number;
    pacientId:number;
    isAdmin:boolean;
    isDentist:boolean;
	constructor($oib: string, $ime: string, $prezime: string, $email: string, password:string|null = null, $mjesto: Mjesto, $isAdmin: boolean, isDentist:boolean = false
        ,dentistId:number, pacientId:number) {
		this.oib = $oib;
		this.ime = $ime;
		this.prezime = $prezime;
		this.email = $email;
		this.mjesto = $mjesto;
		this.isAdmin = $isAdmin;
        this.password = password;
        this.isDentist = isDentist;
        this.dentistId=dentistId;
        this.pacientId=pacientId;
    }

}
class Registracija{
    id:number;
    oib:string;
    ime:string;
    prezime:string;
    email:string;
    password:string|null;
    slikaOsobne:Blob;
    mjesto:Mjesto;
    isDentist:boolean;
    isAdmin:boolean;
    finished:boolean;
    accepted:boolean;
	constructor(id:number, $oib: string, $ime: string, $prezime: string, $email: string, $mjesto: Mjesto, $isAdmin: boolean, password:string|null = null, slikaOsobne:Blob, isDentist:boolean, finished:boolean, accepted:boolean
        ) {
		this.id = id;
        this.oib = $oib;
		this.ime = $ime;
		this.prezime = $prezime;
		this.email = $email;
		this.mjesto = $mjesto;
		this.isAdmin = $isAdmin;
        this.password = password;
        this.slikaOsobne = slikaOsobne;
        this.isDentist = isDentist;
        this.finished = finished;
        this.accepted = accepted;
    }

}
class Mjesto{
    id:number;
    pbrMjesta:number;
    naziv:string;
    constructor(id:number, pbrMjesta:number,naziv:string){
        this.id = id;
        this.pbrMjesta = pbrMjesta;
        this.naziv = naziv;
    }
    static search(search: any, mjesta: Mjesto[]) {
        console.log(search);
        return mjesta.filter(z=>{
            return z.id.toString().includes(        search.id)
        &&     z.pbrMjesta.toString().includes(     search.pbrMjesta)
    &&     z.naziv.toString().includes(             search.naziv)

        })
    }
}

class Pacijent extends Korisnik{
    id:number;
    statusZuba:string;
    zahvati:Zahvat[]
    constructor(id:number, oib: string, ime: string, prezime: string, email: string, password:string, mjesto: Mjesto, isAdmin: boolean,zahvati:Zahvat[], statusZuba:string){
        super(oib, ime, prezime, email,password, mjesto, isAdmin, false, -1, id);
        this.id = id;
        this.zahvati = zahvati;
        this.statusZuba = statusZuba;
    }
}

class Specijalizacija{
    id:number;
    naziv:string;
    constructor(id:number, naziv:string){
        this.id = id;
        this.naziv = naziv;
    }
    static search(search: any, specijalizacije: Specijalizacija[]) {
        console.log(search);
        return specijalizacije.filter(z=>{
            return z.id.toString().includes(     search.id)
            &&     z.naziv.toString().includes(  search.naziv)

        })
    }
}

class Stomatolog extends Korisnik{
    id:number;
    specijalizacija:Specijalizacija
    constructor(id:number, oib: string, ime: string, prezime: string, email: string, password: string, mjesto: Mjesto, isAdmin: boolean,specijalizacija:Specijalizacija){
        super(oib, ime, prezime, email, password, mjesto, isAdmin, true, id, -1);
        this.id = id;
        this.specijalizacija = specijalizacija;
    }
}

class Zahvat{

    serijskiBroj:number;
    datum:Date;
    opis:string;
    idPacijenta:number;

	constructor(serijskiBroj:number, datum:Date, opis:string, idPacijenta:number){
        this.serijskiBroj = serijskiBroj;
        this.datum = datum;
        this.opis = opis;
        this.idPacijenta = idPacijenta;
    }

    static sortBy(variable:string, zahvati:Zahvat[], asc:boolean = true,):Zahvat[]{
        if(['serijskiBroj', 'datum', 'opis', 'idPacijenta'].indexOf(variable)!=-1){
            return zahvati.sort((a,b) => {
                if(a[variable] > b[variable]) return asc?-1:1;
                else if(a[variable] < b[variable]) return asc?1:-1;
                else return 0;
            })
        }
        else return zahvati;
    }
    static search(search: any, zahvati: Zahvat[]) {
        console.log(search);
        return zahvati.filter(z=>{
            return z.idPacijenta.toString().includes(search.idPacijenta)
            &&z.opis.toString().includes(search.opis)
            &&z.datum.toISOString().includes(search.datum)
            &&z.serijskiBroj.toString().includes(search.serijskiBroj)
        })
    }
}

class Tretman{
    id:number;
    naziv:string;
    cijena:number;
    constructor(id:number, naziv:string, cijena:number){
        this.id = id;
        this.naziv = naziv;
        this.cijena = cijena;
    }
    static search(search: any, tretmani: Tretman[]) {
        console.log(search);
        return tretmani.filter(z=>{
            return z.id.toString().includes(     search.id)
            &&     z.naziv.toString().includes(  search.naziv)
            &&     z.cijena.toString().includes( search.cijena)

        })
    }
}

class Termin{
    serijskiBroj:number;
    datum:Date;
    pacijent:Pacijent;
    stomatolog:Stomatolog;
    tretman:Tretman;
    constructor(serijskiBroj:number, datum:Date, pacijent:Pacijent, stomatolog:Stomatolog, tretman:Tretman){
        this.serijskiBroj = serijskiBroj;
        this.datum = datum;
        this.pacijent = pacijent;
        this.stomatolog = stomatolog;
        this.tretman = tretman;
    }
}

class ErrorInfo{
    statusCode:number;
    description:string;
    constructor(statusCode:number, description:string){
        this.statusCode = statusCode;
        this.description = description;
    }
}
export {Registracija, ErrorInfo, Korisnik, Mjesto, Stomatolog, Pacijent, Zahvat, Specijalizacija, Tretman, Termin}