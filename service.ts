import { DAO } from "./dao";
import { IService } from "./iservice";
import { ErrorInfo, Korisnik, Mjesto, Pacijent, Stomatolog, Termin } from "./models";

class Service implements IService{
    private dao:DAO;
    constructor(dao:DAO){
        this.dao = dao;
    }
    fetchKorisnici() {
        return this.dao.getUsers();
    }
    fetchKorisnikByOIB(id: string) {
        return this.dao.getUserByOIB(id);
    }
    updateKorisnik(oib: any, ime: any, prezime: any, email: any, mjesto: any, password: any, dentistId: any, pacientId: any, isAdmin: any) {
        this.dao.updateUserByOIB(oib, ime, prezime, email, password, mjesto, dentistId, pacientId, isAdmin);
    }
    deleteKorisnik(oib: any) {
        this.dao.deleteUserByOIB(oib);
    }
    fetchAllTermini() {
        return this.dao.getAppointments();
    }
    fetchTerminById(id: number) {
        return this.dao.getAppointmentById(id);
    }
    updateTermin(id: any, datum: any, pacijent: any, stomatolog: any, tretman: any) {
        this.dao.updateAppointment(id, datum, pacijent, stomatolog, tretman);
    }
    deleteTermin(id: any) {
        this.dao.deleteAppointmentById(id);
    }
    createTermin(datum: any, pacijent: any, stomatolog: any, tretman: any) {
        this.dao.createAppointment(datum, pacijent, stomatolog, tretman);
    }

    updateTretman(id: any, naziv: any, cijena: any) {
        this.dao.updateTreatment(id, naziv, cijena);
    }
    deleteTretman(id: any) {
        this.dao.deleteTreatmentById(id);
    }
    createTretman(naziv: any, cijena: any) {
        this.dao.createTreatment(naziv, cijena);
    }
    fetchTretmani() {
        return this.dao.getTreatments();
    }
    fetchTretmanById(id: number) {
        return this.dao.getTreatmentById(id);
    }
    
    fetchStomatologByIdBasic(id: number) {
        return this.dao.getDentistByIdBasic(id);
    }
    fetchStomatologBasic() {
        return this.dao.getDentistsBasic();
    }
    fetchStomatologById(id: number) {
        return this.dao.getDentistById(id);
    }
    updateStomatolog(id: any, idSpecijalizacije: any) {
        this.dao.updateDentistById(id, idSpecijalizacije);
    }
    deleteStomatolog(id: any) {
        this.dao.deleteDentistById(id);
    }
    createStomatolog(idSpecijalizacije: any) {
        this.dao.createDentistBasic(idSpecijalizacije);
    }
    fetchSpecijalizacije() {
        return this.dao.getSpecializations();
    }
    fetchSpecijalizacijaById(id: number) {
        return this.dao.getSpecializationById(id);
    }
    updateSpecijalizacija(id: any, naziv:string) {
        this.dao.updateSpecialization(id, naziv);
    }
    deleteSpecijalizacija(id: any) {
        this.dao.deleteSpecializationById(id);
    }
    createSpecijalizacija(naziv: any) {
        this.dao.createSpecialization(naziv);
    }
    fetchUnfinishedRegistrations() {
        return this.dao.getUserRegistrationsUnfinished();
    }
    finishRegistration(id: any, accept: any) {
        this.dao.finishRegistration(id, accept);
    }
    ValidateEmail(mail) 
    {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        return (true)
    }
        return (false)
    }
    ValidateOib(oib) 
    {
        let regex = /[0-9]+/i;
        return regex.test(oib);
    }
    async createRegistration(oib: string, name: any, surname: any, email: any, password: any, idcard: any, placeId: any) {
        var err = new ErrorInfo(1,"");
        console.log(oib, name, surname, email, password, idcard, placeId);
        
        if(!oib || !name|| !surname|| !email|| !password|| !idcard|| !placeId){
            err.statusCode=0;
            err.description+="Fields must not be empty. \n";
        }
        if(!this.ValidateOib(oib)){
            err.statusCode=0;
            err.description+="Oib must only have numbers. \n";
        }
        if(oib.length != 11){
            err.statusCode=0;
            err.description+="Oib must have 11 numbers. \n";
        }
        if(oib && !(await this.dao.getUserByOIB(oib) instanceof ErrorInfo)){
            err.statusCode=0;
            err.description+="This oib is already in use. \n";
        }
        
        if(!this.ValidateEmail(email)){
            err.statusCode=0;
            err.description+="Invalid email address. \n";
        }

        if(err.statusCode==1)
            this.dao.createUserRegistration(oib, name, surname, email, password, placeId, idcard, false);
        return err;    
    }
    fetchZahvati() {
        return this.dao.getProcedures();
    }
    fetchZahvatById(id: number) {
        return this.dao.getProcedureById(id);
    }
    updateZahvat(id, date, desc, pacientId) {
        this.dao.updateProcedureById(id, date, desc, pacientId)
    }
    deleteZahvat(id: any) {
        this.dao.deleteProcedureById(id);
    }
    createZahvat(date, desc, pacientId) {
        this.dao.createProcedure(date, desc, pacientId)
    }
    fetchPacijenti() {
        return this.dao.getPacientsBasic();
    }
    fetchPacijentById(id: number) {
        return this.dao.getPacientByIdBasic(id);
    }
    updatePacijent(id, statusZuba) {
       this.dao.updatePacientById(id, statusZuba)
    }
    deletePacijent(id) {
        this.dao.deletePacientById(id);
    }
    createPacijent(statusZuba:string) {
        this.dao.createBasicPacient(statusZuba);
    }
    fetchMjestoById(id: number) {
        return this.dao.getPlaceById(id);
    }
    updateMjesto(mjesto: Mjesto) {
        this.dao.updatePlaceById(mjesto.id, mjesto.pbrMjesta, mjesto.naziv);
    }
    deleteMjesto(mjesto: Mjesto) {
        this.dao.deletePlaceById(mjesto.id);
    }
    createMjesto(mjesto: Mjesto) {
        this.dao.createPlace(mjesto.pbrMjesta, mjesto.naziv);
    }
    fetchMjesta(): Promise<Mjesto[]> {
        return this.dao.getPlaces();
    }
    async fetchStomatolozi(): Promise<Stomatolog[]> {
        return this.dao.getDentists();
    }
    getUserByOIB(oib: string): Promise<Korisnik | ErrorInfo> {
        return this.dao.getUserByOIB(oib);
    }
    fetchTermini(user: Korisnik, isDentist: boolean): Promise<Termin[]> {
        if(isDentist)
            return this.dao.getAppointmentsForDentistById(user.dentistId);
        else
            return this.dao.getAppointmentsForPacientById(user.pacientId);
    }
    async login(email:string, password:string, isDentist:boolean){
        var u:Korisnik|ErrorInfo = await this.dao.getUserByEmail(email);

        console.log("serv");
        console.log(u);
        
        if(u instanceof ErrorInfo){
            u.description="wrong username or password";
            u.statusCode=401;
            return u;
        }else{
            u.isDentist = isDentist;
            var err:ErrorInfo = new ErrorInfo(401, "wrong");
            err.description="wrong username or password";
            err.statusCode=401;
            if(u.password == password) return u;
            else return err;
        }
    }
    async getUser(oib:string) : Promise<Korisnik|ErrorInfo>{
        var res:Korisnik|ErrorInfo = await this.dao.getUserByOIB(oib);
        if(res instanceof ErrorInfo){
            return res;
        }else {
            return res;
        }
    }

}

export {Service}