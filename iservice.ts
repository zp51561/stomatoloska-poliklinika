import { ErrorInfo, Korisnik, Mjesto, Pacijent, Stomatolog, Termin } from "./models";

interface IService{
    fetchKorisnici();
    fetchKorisnikByOIB(id: string);
    updateKorisnik(oib: any, ime: any, prezime: any, email: any, mjesto: any, password: any, dentistId: any, pacientId: any, isAdmin: any);
    deleteKorisnik(oib: any);
    fetchAllTermini();
    fetchTerminById(id: number);
    updateTermin(id: any, datum: any, pacijent: any, stomatolog: any, tretman: any);
    deleteTermin(id: any);
    createTermin(datum: any, pacijent: any, stomatolog: any, tretman: any);
    updateTretman(id: any, naziv: any, cijena: any);
    deleteTretman(id: any);
    createTretman(naziv: any, cijena: any);
    fetchTretmani();
    fetchTretmanById(id: number);
  
    fetchStomatologByIdBasic(id: number);
    fetchStomatologById(id: number);
    fetchStomatologBasic();
    updateStomatolog(id: any, idSpecijalizacije: any);
    deleteStomatolog(id: any);
    createStomatolog(idSpecijalizacije: any);
    fetchSpecijalizacije();
    fetchSpecijalizacijaById(id: number);
    updateSpecijalizacija(id: any, naziv);
    deleteSpecijalizacija(id: any);
    createSpecijalizacija(naziv: any);
    fetchUnfinishedRegistrations();
    finishRegistration(id: any, accept: any);
    fetchZahvati();
    fetchZahvatById(id: number);
    fetchTretmani();
    updateZahvat(id, date, desc, pacientId);
    deleteZahvat(id: any);
    createZahvat(date, desc, pacientId);
    fetchPacijenti();
    fetchPacijentById(id: number) 
    updatePacijent(id, statusZuba)
    deletePacijent(id) 
    createPacijent(statusZuba: string);
    fetchMjestoById(id: number);
    updateMjesto(mjesto: Mjesto);
    deleteMjesto(mjesto: Mjesto);
    createMjesto(mjesto: Mjesto);
    fetchMjesta() : Promise<Mjesto[]>;
    createRegistration(oib,name,surname,email,password,idcard,placeId);
    login(email:string, password:string, isDentist:boolean);
    getUser(oib:string) : Promise<Korisnik|ErrorInfo>
    fetchTermini(user:Korisnik, isDentist:boolean) : Promise<Termin[]>;
    getUserByOIB(oib:string) : Promise<Korisnik|ErrorInfo>;
    fetchStomatolozi() : Promise<Stomatolog[]>;
}

export {IService}