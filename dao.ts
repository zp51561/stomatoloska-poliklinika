import { ErrorInfo, Korisnik, Mjesto, Pacijent, Tretman } from './models';

interface DAO{
    getAppointments();
    getDentistByIdBasic(id);
    createDentistBasic(idSpecijalizacije: any);
    getProcedures();

    getUserByOIB(oib:string);//:Promise<Korisnik|ErrorInfo>|Korisnik|ErrorInfo
    getUserByEmail(email:string);//:Promise<Korisnik|ErrorInfo>|Korisnik|ErrorInfo
    getUsers();//:Promise<Korisnik[]|ErrorInfo>|Korisnik[]|ErrorInfo
    getUsersPaged(pagenum:number, perpage:number);//:Promise<Korisnik[]|ErrorInfo>|Korisnik[]|ErrorInfo
    deleteUserByOIB(oib:string);
    updateUserByOIB(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta:number, idStomatologa:number, idPacijenta:number, administrator:boolean);

    createUserRegistration(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta:number, slikaOsobne:Blob, isDentist:boolean);
    getUserRegistrations();
    getUserRegistrationById(id:number);
    getUserRegistrationByOIB(oib:string);
    getUserRegistrationsUnfinished();
    finishRegistration(id:number, accept:boolean);

    getPacientById(id:number);//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getPacientByIdBasic(id:number);//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getPacients();//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getPacientsBasic();//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getPacientByOIB(oib:string);//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    createPacient(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta:number);//:Promise<ErrorInfo>|ErrorInfo
    createBasicPacient(statusZuba:string);//:Promise<ErrorInfo>|ErrorInfo
    
    deletePacientByOIB(oib:string);
    deletePacientById(id:number);
    updatePacientById(id:number, statusZuba:string)

    getDentistById(id:number);//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getDentists();//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getDentistsBasic();//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    getDentistByOIB(oib:string);//:Pacijent|ErrorInfo|Promise<Pacijent|ErrorInfo>
    createDentist(oib: string, ime: string, prezime: string, email: string, password:string, idMjesta:number, idSpecijalizacije:number)//:Promise<ErrorInfo>|ErrorInfo
    deleteDentistByOIB(oib:string);
    deleteDentistById(id:number);
    updateDentistById(id:number, idSpecijalizacije:number);
    
    getPlaces();//:Promise<Mjesto[]|ErrorInfo>|Mjesto[]|ErrorInfo
    getPlaceById(id:number);//:Promise<Mjesto|ErrorInfo>|Mjesto|ErrorInfo
    getPlaceByPbr(pbrMjesta:number);//:Promise<Mjesto|ErrorInfo>|Mjesto|ErrorInfo
    createPlace(pbrMjesta:number, naziv:string);//:Promise<ErrorInfo>|ErrorInfo
    deletePlaceById(id:number);
    deletePlaceByPbr(pbrMjesta:number);
    updatePlaceById(id:number, pbrMjesta:number, naziv:string);
    updatePlaceByPbr(pbrMjesta:number, naziv:string);
    
    getProcedureById(id:number);
    getProceduresFromPacient(pacientId:number);
    createProcedure(date:Date, desc: string, pacientId:number);
    deleteProcedureById(id:number);
    deleteProcedureByPacientId(pacientId:number);
    updateProcedureById(id:number, date:Date, desc: string, pacientId:number);
    
    getAppointmentById(id:number);
    getAppointmentsForPacientById(pacientId:number);
    getAppointmentsForDentistById(dentistId:number);
    getAppointmentsForTreatmentById(treatmentId:number);
    getAppointmentsByDate(date:Date);
    createAppointment(date:Date, pacientId:number, dentistId:number, treatmentId:number);
    deleteAppointmentById(id:number);
    deleteAppointmentsForPacientById(pacientId:number);
    deleteAppointmentsForDentistById(dentistId:number);
    deleteAppointmentsForTreatmentById(treatmentId:number);
    deleteAppointmentsByDate(date:Date);
    updateAppointment(id:number, date:Date, pacientId:number, dentistId:number, treatmentId:number)
    
    getSpecializationById(id:number);
    getSpecializations();
    createSpecialization(name:string);
    deleteSpecializationById(id:number);
    updateSpecialization(id:number, name:string);
    

    getTreatmentById(id:number);//:Promise<Tretman|ErrorInfo>|Tretman|ErrorInfo
    getTreatments();//:Promise<Korisnik[]|ErrorInfo>|Korisnik[]|ErrorInfo
    getTreatmentsPaged(pagenum:number, perpage:number);//:Promise<Korisnik[]|ErrorInfo>|Korisnik[]|ErrorInfo
    createTreatment(name:string, price:number);
    deleteTreatmentById(id:number);
    updateTreatment(id:number, name:string, price:number);
    
    init():void;
    createDB():void;
    populate():void;
}
export {DAO}